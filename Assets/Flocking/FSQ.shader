﻿Shader "Unlit/FSQ"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"


            sampler2D _MainTex;
            float4 _MainTex_ST;

			struct vertInput
			{
				float4 pos: POSITION;
				float2 UV:  TEXCOORD0;

			};

			struct v2f {
				float4  pos : SV_POSITION;
				float2  uv : TEXCOORD0;
			};


			v2f vert(vertInput IN)
			{
				v2f OUT;
				OUT.pos = IN.pos;
				OUT.pos.xy *= 2;
				OUT.pos.z = 1;

				OUT.uv = IN.UV;
				return OUT;
			}

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog

                return col;
            }
            ENDCG
        }
    }
}
