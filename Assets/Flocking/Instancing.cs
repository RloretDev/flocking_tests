﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

#pragma warning disable CS0649

public class Instancing : MonoBehaviour
{


    [SerializeField]private Mesh _meshToInstance;
    [SerializeField]private Material _instanceMaterial;
    [SerializeField]private ComputeShader _computeMaterial;


    [Header("Shader Variables Names")]
    [SerializeField]private  int   _instanceCount;
    [SerializeField] private float _instanceMaxLinearSpeed;
    [SerializeField] private float _instanceMaxForce;
    [SerializeField] private float _instanceAngularSpeed;
    [SerializeField] private float _instancesSeparationRadius;




    GPUInstancer<Enemy> instancer;
    private void Start()
    {
        instancer = new GPUInstancer<Enemy>(_meshToInstance, _instanceMaterial,_computeMaterial, _instanceCount);
       // Camera.main.depthTextureMode = DepthTextureMode.Depth ;
    }

    public void Update()
    {
        Func<string, string> stringcutter = (string a) => {
            StringBuilder cut = new StringBuilder(a);
            int length = a.Length;
            return cut.ToString(1,length-1);
        };

        Func<object,string, Tuple<string,object>> tupleComposer = (object b,string a) =>
        {
            return new Tuple<string, object>( stringcutter(a), b);
        };

        //crea una lista de tuplas
        var tempargs = new List< Tuple<string, object>>();

        float _deltaTime = Time.deltaTime ;

        tempargs.Add(tupleComposer(_instanceCount            , nameof(_instanceCount)));
        tempargs.Add(tupleComposer(_instanceMaxLinearSpeed      , nameof(_instanceMaxLinearSpeed      )));
        tempargs.Add(tupleComposer(_instanceMaxForce, nameof(_instanceMaxForce)));

        tempargs.Add(tupleComposer(_instanceAngularSpeed     , nameof(_instanceAngularSpeed     )));
        tempargs.Add(tupleComposer(_instancesSeparationRadius, nameof(_instancesSeparationRadius)));
        tempargs.Add(tupleComposer(_deltaTime,                 nameof(_deltaTime)));


        //se mandan como parametros al instanciador que los usa para dibujar
        instancer.Compute(tempargs.ToArray());

    }

    private void LateUpdate()
    {

        instancer.Draw();
    }


    public void OnDestroy()
    {
        instancer.Dispose();
    }



}


public class GPUInstancer<T> where T: IInitializable,new() {


    private Mesh     _meshToInstance;
    private Material _instanceMaterial;
    private ComputeShader _computeMaterial;
    private int      _instanceCount;


    private ComputeBuffer _argsbuffer;
    private LinearComputeWrapper<T> _enemyComputeWrapper;

    public ComputeShader ComputeMaterial { get => _computeMaterial; set => _computeMaterial = value; }
    public Mesh MeshToInstance { get => _meshToInstance; set => _meshToInstance = value; }
    public Material InstanceMaterial { get => _instanceMaterial; set => _instanceMaterial = value; }
    public int InstanceCount { get => _instanceCount; set => _instanceCount = value; }

    public GPUInstancer(Mesh meshToInstance, Material instanceMaterial,ComputeShader computeMaterial, int instanceCount)
    {
        _meshToInstance = meshToInstance;
        _instanceMaterial = instanceMaterial;
        _instanceCount = instanceCount;
        _computeMaterial = computeMaterial;
        Init();
    }

    public void Init()
    {
        uint[] args = new uint[5] { 0, (uint)_instanceCount, 0, 0, 0 };

        if (_meshToInstance != null)
        {
            args[0] = (uint)_meshToInstance.GetIndexCount(0);
            args[1] = (uint)_instanceCount;
            args[2] = (uint)_meshToInstance.GetIndexStart(0);
            args[3] = (uint)_meshToInstance.GetBaseVertex(0);
        }
        _argsbuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);

        _argsbuffer.SetData(args);
        _enemyComputeWrapper = new LinearComputeWrapper<T>(_instanceCount, _computeMaterial);


    }

    // Update is called once per frame
    public void Compute(Tuple<string,object>[] args)
    {

        foreach (var tuple in args)
        {
            _enemyComputeWrapper.SetShaderValue(tuple.First, tuple.Second);
        }

        _instanceMaterial.SetBuffer("FlockData", _enemyComputeWrapper.SetBufferData());
        _enemyComputeWrapper.Dispatch();
        
    }

    public void Draw()
    {

        Graphics.DrawMeshInstancedIndirect(_meshToInstance, 0, _instanceMaterial, new Bounds(Vector3.zero, new Vector3(100.0f, 100.0f, 100.0f)), _argsbuffer);

    }

    public  void Dispose()
    {
        _argsbuffer.Release();
        _enemyComputeWrapper.Dispose();
    }

}

public class Tuple<T, U> {
    private T _first;
    private U _second;

    public Tuple(T first, U second)
    {
        _first = first;
        _second = second;
    }

    public T First { get => _first; set => _first = value; }
    public U Second { get => _second; set => _second = value; }

    
}

public interface IInitializable
{
    void Init();
}

public struct Enemy: IInitializable 
{
    public Vector3 position;
    public Vector3 direction;
    public float speed;
    public float scale;

    public void Init()
    {
        position = new Vector3(UnityEngine.Random.Range(-1.0f,1.0f),0, UnityEngine.Random.Range(-1.0f,1.0f))*5;
        
        direction = UnityEngine.Random.insideUnitSphere;
        direction.y = 0;
        direction = direction.normalized;

        speed = UnityEngine.Random.Range(0.5f, 2f) ;
        scale =     UnityEngine.Random.Range(0.6f, 1f);
       

    }
}



/// <summary>
/// Compute shader for a 1D compute shader with blocksize fixed to maximum and blocksize variable.
/// </summary>
/// <typeparam name="T"></typeparam>
class LinearComputeWrapper<T> where T : IInitializable,new()
{
    private T[] _dataBuffer;

    private ComputeBuffer _dataCBuffer;
    private ComputeShader _computeShader;

    

    private const int _groupSize =256;
    private       int _blockSize ;
    private       int _kernelId;

    bool _constantsSet = false;

    private const string _kernelName = "BoidMovementKernel";

    public ComputeBuffer DataComputeBuffer { get => _dataCBuffer; set => _dataCBuffer = value; }

    public LinearComputeWrapper(int instanceCount, ComputeShader shader)
    {
        this._computeShader = shader;
        _kernelId = shader.FindKernel(_kernelName);
        configureComputeThreads(out _blockSize, instanceCount);
        createData(instanceCount);

    }

    public void SetShaderValue (string key, object value) {

        if (value.GetType() == typeof(float)) SetShaderValue( key, (float)value);
        else if (value.GetType() == typeof(int)) SetShaderValue( key, (int)value);
    }

    public void SetShaderValue(string key, float value)
    {
       _computeShader.SetFloat(key, value);
    }
            
    public void SetShaderValue(string key, int value)
    {
        _computeShader.SetInt(key, value);
    }

    private void configureComputeThreads ( out int groupSize, int amount){
        float tempgroupSize = (amount * 1.0f) / _blockSize;
        groupSize = Mathf.FloorToInt(tempgroupSize) + 1;

    }

   public ComputeBuffer SetBufferData()
   {
       _computeShader.SetBuffer(_kernelId, "FlockData", _dataCBuffer);   
       return _dataCBuffer;
   }

    private void createData(int amount) {
        _dataBuffer = new T[amount];
        for (int i = 0; i < amount; i++)
        {
            _dataBuffer[i] = new T();
            _dataBuffer[i].Init();
        }

        _dataCBuffer = new ComputeBuffer(amount, Marshal.SizeOf(typeof(T)));
        _dataCBuffer.SetData(_dataBuffer);
    }

    public void Dispose()
    {
        _dataCBuffer.Release();
    }

    public void Dispatch()
    {
        _computeShader.Dispatch(_kernelId, _groupSize, 1, 1);

        GL.Flush(); //make the kernel execute as a synchronous call;
    }

    


}