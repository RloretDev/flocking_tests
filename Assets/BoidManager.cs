﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidManager : MonoBehaviour
{

    public int instanceMaxLinearSpeed;
    public int instanceMaxForce;
    public int instanceAngularSpeed;
    public int instancesSeparationRadius;
    public int limit;
    public Boid[] Boids;
    // Start is called before the first frame update
    void Awake()
    {
        Boid.instanceMaxLinearSpeed=instanceMaxLinearSpeed; 
        Boid.instanceMaxForce      =instanceMaxForce; 
        Boid.instanceAngularSpeed  = instanceAngularSpeed; 
        Boid.instancesSeparationRadius = instancesSeparationRadius;
        Boid.limit = limit;

    }

    // Update is called once per frame
    void Update()
    {
        Boid.instanceMaxLinearSpeed = instanceMaxLinearSpeed;
        Boid.instanceMaxForce = instanceMaxForce;
        Boid.instanceAngularSpeed = instanceAngularSpeed;
        Boid.instancesSeparationRadius = instancesSeparationRadius;
        Boid.limit = limit;


        for (int i = 0; i < Boids.Length; i++)
        {
            if(i<=limit)
                Boids[i].computeFlocking(Boids);
            else
            {
                Boids[i].gameObject.SetActive(false);
            }
        }

    }
}
