﻿Shader "Stering/InstanceSteering"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
	   Tags { "Queue" = "Geometry" "IgnoreProjector" = "True" }
       

        Pass
        {
			//Blend SrcAlpha OneMinusSrcAlpha
			//ColorMask RGB
			//Cull Off Lighting Off ZWrite Off


            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing
			#pragma target 4.5

            #include "UnityCG.cginc"



			CBUFFER_START(Params)
			float deltaTime;
			
			CBUFFER_END



#if SHADER_TARGET >= 45

			struct FlockUnit {

				float3 position;
				float3 direction;
				float speed;
				float scale;
			};
			StructuredBuffer<FlockUnit> FlockData;
#endif



            struct appdata
            {
				uint instanceID : SV_InstanceID;
				UNITY_VERTEX_INPUT_INSTANCE_ID

                float4 vertex : POSITION;
				float3 normal: NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct vertexOutput
            {
				uint instanceID:SV_InstanceID;
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 normal: NORMAL;

            };

			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;



			#define Unit FlockData[v.instanceID]
			#define S(a) sin(a)
			#define C(a) cos(a)
			#define Rot2D(a) float2x2(C(a),-S(a),S(a),C(a))
			#define pi acos(-1.)
            vertexOutput vert (appdata v)
            {
                vertexOutput o;

				float3x3 lookat = float3x3(1, 0, 0, 0, 1, 0, 0, 0, 1);
				 #if SHADER_TARGET >= 45
					float3 position = Unit.position;

					float scale = Unit.scale;

					float3 tmpup = float3(0, 1, 0);
					float3 forward = Unit.direction;
					float3 right = normalize(cross(tmpup, forward));
					float3 up = normalize(cross(forward, right)); 
					lookat = float3x3(right.x, up.x, forward.x,
									  right.y, up.y, forward.y,
									  right.z, up.z, forward.z);


				 #else
					float3 position = float3(v.instanceID,0,0);
					float3 d = 1;
					float scale = 1;
				 #endif

				
				o.vertex = v.vertex;
				o.vertex*= scale;
				//o.vertex.zy = mul(Rot2D(pi*0.5), o.vertex.zy);
				o.vertex = float4(mul(lookat, o.vertex.xyz),1);
				o.vertex.xyz += position;
				o.vertex = UnityObjectToClipPos(o.vertex);
				
                o.uv = v.uv;
				o.normal = v.normal;

				return o;
            }

            float4 frag (vertexOutput i) : SV_Target
            {
				float2 tuv = i.uv;
				tuv = tuv * 2 - 1;
				float l = 1-length(tuv) ;

                return  float4(i.normal,1);
            }
            ENDCG
        }
    }
}
