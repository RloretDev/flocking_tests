﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    public static int instanceMaxLinearSpeed;
    public static int instanceMaxForce;
    public static int instanceAngularSpeed;
    public static int instancesSeparationRadius;
    public static int limit;
    public Enemy Unit;
    public  int id;
    public bool verbose;

    // Start is called before the first frame update
    void Start()
    {
        Unit = new Enemy();
        Unit.Init();
        id = this.GetInstanceID();
        this.transform.position = Unit.position;
        this.transform.localScale = Unit.scale * Vector3.one;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(this.transform.position,this.transform.position + this.transform.forward);
        Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.2f);
        Gizmos.DrawSphere(this.transform.position, instancesSeparationRadius);
    }
    public void computeFlocking(Boid[] enemies)
    {

        Enemy Cohesion = new Enemy();
        Cohesion.Init();
        Cohesion.position = Vector3.zero;
        Cohesion.scale = Unit.scale;
        Vector3 fSeparation = Vector3.zero, fCohesion = Vector3.zero, fAlignment = Unit.direction;
         int count = 1;
        for (int j = 0; j < limit; j++)
        {
            Enemy Neighbour = enemies[j].Unit;

            if (id == enemies[j].id)
            {
              
                continue;
            }

            float d = Vector3.Distance(Unit.position, Neighbour.position);
            float cosdir = Vector3.Dot((Neighbour.position - Unit.position) / d, Unit.direction);
            if (d <= (instancesSeparationRadius + Unit.scale + Neighbour.scale)
                /*&& cosdir > Mathf.Cos(45 * Mathf.PI / 180.0f)*/)
            {
                Cohesion.position += Neighbour.position;
                Cohesion.scale += Neighbour.scale;
                Vector3 DtoEnemy = Neighbour.position - Unit.position;
               
                fSeparation += Leave(ref Unit, ref Neighbour);//(-DtoEnemy / d) * (1.0-d/ instancesSeparationRadius) * instanceMaxForce;
                fAlignment +=  Neighbour.direction;
                count += 1;

            }
        }




        int mask = count > 1 ? 1 : 0;
        float avg = 1.0f / count;
        fSeparation *= avg;

        Cohesion.position += Unit.position * mask;
        Cohesion.scale += Unit.scale * mask;

        Cohesion.scale *=avg;
        Cohesion.position *= avg;
       
       // Debug.DrawLine(Unit.position, Cohesion.position,Color.black);


        fCohesion = (Arrive(ref Unit, ref Cohesion)) ;

        //fCohesion.Normalize();
        //fSeparation.Normalize();
        fAlignment.Normalize();
        fAlignment *= instanceMaxLinearSpeed;
        //Vector3 fWander = computeWander2D(Unit.position, 10 + Unit.scale);
        //Vector3 fReturn = -Unit.direction * smoothstep(0,20.0, length(Unit.position));
        Vector3 direction =   fSeparation ;// (fSeparation + fCohesion + fAlignment);//(fCohesion) + normalize(fSeparation) +  normalize(fWander) + normalize(fAlignment);
                                         // if(verbose) Debug.Log(fSeparation);

       if(verbose) Debug.Log(direction);
        // Debug.DrawLine(Unit.position, Cohesion.position, mask==1?Color.green: Color.red);
        //Debug.DrawLine(Unit.position, Unit.position + fAlignment, Color.magenta);
        float L = (direction).magnitude;
        float F = Mathf.Clamp(L,0, instanceMaxForce);
        if(verbose)Debug.Log((direction).magnitude);
        float a = F / Unit.scale;
        direction.y = 0;

       if(L!=0.0) Unit.direction = Vector3.Lerp((direction).normalized, Unit.direction, Mathf.Exp(-instanceAngularSpeed / (Unit.scale) * Time.deltaTime));
        Debug.DrawLine(Unit.position, Unit.position + Unit.direction, Color.red);


        //Unit.speed += a * Time.deltaTime;
        Unit.speed = Mathf.Clamp( L ,0, instanceMaxLinearSpeed)*0.99f;


        Unit.position += Unit.speed * Unit.direction * Time.deltaTime;
        Unit.direction = (Unit.direction).normalized;
        this.transform.position = Unit.position;
        this.transform.LookAt(this.transform.position + direction);

    }



    Vector3 Seek(ref Enemy current, ref Enemy neighbour)
    {
        Vector3 desiredV = neighbour.position - current.position;
        float d = (desiredV).magnitude;
        desiredV = desiredV / d * instanceMaxLinearSpeed;
        Vector3 currSpeed = current.direction * current.speed;

        return desiredV - currSpeed;
    }

    Vector3 Leave(ref Enemy current, ref Enemy neighbour)
    {
        Vector3 desiredV = current.position - neighbour.position;
        float d = (desiredV).magnitude;

        float unsafeRadious =instancesSeparationRadius *0.5f + Unit.scale ;

        float acceleratingDist = (neighbour.scale + current.scale);

        bool insideDanger = d < unsafeRadious;
        Vector3 currSpeed = current.direction * current.speed;
        if (d < unsafeRadious)
        {
            desiredV = (desiredV / d) * instanceMaxLinearSpeed;

            Debug.DrawLine(Unit.position, neighbour.position, Color.red);
        }
        else
        {
            float percent = (d + unsafeRadious) / (acceleratingDist + unsafeRadious);
            if (verbose) Debug.Log(percent);
            desiredV = (desiredV / d) * instanceMaxLinearSpeed * ( 1.0f - Mathf.Clamp01(percent) );

           // Debug.DrawLine(Unit.position, neighbour.position, Color.green);
        }
        

        return desiredV- currSpeed;

    }


    Vector3 Pursue(ref Enemy current, ref Enemy neighbour)
    {

        float distance = (neighbour.position - current.position).magnitude;
        float ahead = distance * Time.deltaTime;
        Vector3 futurePosition = neighbour.position + neighbour.direction * neighbour.speed * ahead;
        Enemy futureflock;

        futureflock = neighbour;
        futureflock.position = futurePosition;
        return Seek(ref current, ref futureflock);
    }

    Vector3 Arrive(ref Enemy current, ref Enemy neighbour)
    {
        Vector3 desiredV = neighbour.position - current.position;

        float d = (desiredV).magnitude;
        if (d == 0) return Vector3.zero;
        float safeRadious = neighbour.scale;

        float slowingDistance = neighbour.scale + current.scale ;

        bool insideRadious = d < safeRadious;

        desiredV = (desiredV / d) * instanceMaxLinearSpeed * (insideRadious ?0: ((d +safeRadious) / (slowingDistance + safeRadious)) );

      

        Vector3 currSpeed = current.direction * current.speed;
    
        return  desiredV ;
    }

}
