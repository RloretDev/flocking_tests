﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class test_angle : MonoBehaviour
{

    public GameObject other;
    public float instanceAngularSpeed = 0;
    public float instanceMaxLinearSpeed=2;
    public float instanceMaxForce = 2;
    public float instancesSeparationRadius = 4;

    Enemy[] enemies = new Enemy[1];

    private void OnDrawGizmos()
    {
        if (enemies.Length==1)
        {
            enemies = new Enemy[10];
            for (int i = 0; i < 10; i++)
            {
                enemies[i].Init();
              
            }

        }
        else
        {

            for (int i = 0; i < 10; i++)
            {
                Enemy Unit = enemies[i];
                Vector3 fSeparation = Unit.direction, fCohesion = Unit.direction, fAlignment = Unit.direction;
                int count = 1;

                for (int j = 0; j < 10; j++)
                {
                    if (i == j) continue;

                    Enemy Neighbour = enemies[j];
                    float d = Vector3.Distance(Unit.position, Neighbour.position);
                    float cosdir = Vector3.Dot((Neighbour.position - Unit.position) / d, Unit.direction);
                    if (d <= (instancesSeparationRadius) && d != 0)
                    {

                        Vector3 DtoEnemy = Neighbour.position - Unit.position;
                        Gizmos.color = Color.red;
                       // Gizmos.DrawSphere(Neighbour.position, Neighbour.scale * 4) ;
                        fSeparation += -Pursue(ref Unit, ref Neighbour);//(-DtoEnemy / d) * (1.0-d/ instancesSeparationRadius) * instanceMaxForce;
                        fCohesion += Neighbour.position;
                        fAlignment += cosdir > Mathf.Cos(45 * Mathf.PI / 180.0f) ? Neighbour.direction : Vector3.zero;
                        count += 1;
                    }

                }


                int mask = count > 1?1:0;
                float avg = 1.0f / count;
                fSeparation *= avg;
                fCohesion *= avg;

                Enemy Cohesion = Unit;
                Cohesion.position = fCohesion;
                Gizmos.DrawSphere(Cohesion.position, 0.1f);
                drawString("Cohesion Point", Cohesion.position + Vector3.up * Cohesion.scale, Color.yellow);

                fCohesion = (Arrive(ref Unit, ref Cohesion)) * mask;

                //Vector3 fWander = computeWander2D(Unit.position, 10 + Unit.scale);
                //Vector3 fReturn = -Unit.direction * smoothstep(0,20.0, length(Unit.position));
                Vector3 direction = (fSeparation + fCohesion).normalized;//(fCohesion) + normalize(fSeparation) +  normalize(fWander) + normalize(fAlignment);
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(Unit.position, Unit.position + fSeparation);
                //Gizmos.DrawLine(Unit.position, Unit.position + fAlignment);
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(Unit.position, Unit.position + fCohesion);
                //Gizmos.DrawLine(Unit.position, Unit.position + direction);
                Gizmos.color = Color.red;
                float F = Mathf.Min((direction).magnitude, instanceMaxForce);

                float a = F / Unit.scale;


                Unit.direction = Vector3.Lerp((direction).normalized, Unit.direction, Mathf.Exp(-instanceAngularSpeed / (Unit.scale) * Time.deltaTime));

                Unit.speed += a * Time.deltaTime;
                Unit.speed = Mathf.Min(Unit.speed, instanceMaxLinearSpeed);


                Unit.position += Unit.speed * Unit.direction * Time.deltaTime;
                Unit.direction = (Unit.direction).normalized;
                enemies[i] = Unit;
                Gizmos.color = Color.gray;
                Gizmos.DrawSphere(Unit.position, Unit.scale);
                Gizmos.color = new Color(0, 0, 1, 0.3f);
                Gizmos.DrawSphere(Unit.position, instancesSeparationRadius);


            }
        }
        drawCone();
    }

    Vector3 Seek(ref Enemy current, ref Enemy neighbour)
    {
        Vector3 desiredV = neighbour.position - current.position;
        float d = (desiredV).magnitude;
        desiredV = desiredV / d * instanceMaxLinearSpeed;
        Vector3 currSpeed = current.direction * current.speed;

        return desiredV - currSpeed;
    }


    static void drawString(string text, Vector3 worldPos, Color? colour = null)
    {
        UnityEditor.Handles.BeginGUI();
        if (colour.HasValue) GUI.color = colour.Value;
        var view = UnityEditor.SceneView.currentDrawingSceneView;
        Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
        Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
        GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
        UnityEditor.Handles.EndGUI();
    }


    Vector3 Pursue(ref Enemy current, ref Enemy neighbour)
    {

        float distance = (neighbour.position - current.position).magnitude;
        float ahead = distance * Time.deltaTime;
        Vector3 futurePosition = neighbour.position + neighbour.direction * neighbour.speed * ahead;
        Enemy futureflock;

        futureflock = neighbour;
        futureflock.position = futurePosition;
        return Seek(ref current,ref futureflock);
    }

    Vector3 Arrive(ref Enemy current, ref Enemy neighbour)
    {
        Vector3 desiredV = neighbour.position - current.position;
        float d = (desiredV).magnitude;

        float slowingDistance = neighbour.scale + current.scale;
        

        desiredV = (desiredV / d) * instanceMaxLinearSpeed * (d < slowingDistance ? (d / slowingDistance) :1.0f );

        Vector3 currSpeed = current.direction * current.speed;

        return desiredV - currSpeed;
    }

    public void drawCone()
    {
        Gizmos.color = Color.yellow;
        Vector3 F = Quaternion.Euler(0, 45, 0) * this.transform.forward * 4;


        Gizmos.DrawLine(this.transform.position, this.transform.position + F);
        F = Quaternion.Euler(0, -45, 0) * this.transform.forward * 4;

        Gizmos.DrawLine(this.transform.position, this.transform.position + F);

        //Vector3 D = -this.transform.position + other.transform.position;
        //D = D.normalized;
        //Gizmos.DrawLine(this.transform.position, this.transform.position + D);

       //float c = Mathf.Cos(Mathf.Deg2Rad * 45.0f);
       //float d = Vector3.Dot(this.transform.forward, D);
       //Gizmos.color = d > c ? Color.green : Color.red;
       //Gizmos.DrawSphere(this.transform.position, 0.6f);
    }

}





