﻿// Each #kernel tells which function to compile; you can have many kernels

#include "UnityCG.cginc"

#pragma kernel BoidMovementKernel
#define GroupSize 256
#define G 9.8

struct FlockUnit {

	float3 position;
	float3 direction;
	float scale;
	float speed;
};

RWStructuredBuffer<FlockUnit> FlockData;

CBUFFER_START(Params)
	uint  instanceCount;
	float deltaTime;
	float instancesSeparationRadius;
	float instanceLinearSpeed;
	float instanceAngularSpeed;
	
CBUFFER_END


float hash(float3 p)  // replace this by something better
{
	p = frac(p*0.3183099 + .1);
	p *= 17.0;
	return frac(p.x*p.y*p.z*(p.x + p.y + p.z));
}

float noise(in float3 x)
{
	float3 p = floor(x);
	float3 f = frac(x);

	f = f * f*(3.0 - 2.0*f);
	float n = p.x + p.y*57.0 + 113.0*p.z;

	return lerp(lerp(lerp(hash(n + 0.0), hash(n + 1.0), f.x),
		lerp(hash(n + 57.0), hash(n + 58.0), f.x), f.y),
		lerp(lerp(hash(n + 113.0), hash(n + 114.0), f.x),
			lerp(hash(n + 170.0), hash(n + 171.0), f.x), f.y), f.z);
}


float hash(float2 p)  // replace this by something better
{
	p = 50.0*frac(p*0.3183099 + float2(0.71, 0.113));
	return -1.0 + 2.0*frac(p.x*p.y*(p.x + p.y));
}

float noise(in float2 p)
{
	float2 i = floor(p);
	float2 f = frac(p);

	float2 u = f * f*(3.0 - 2.0*f);

	return lerp(lerp(hash(i + float2(0.0, 0.0)),
		hash(i + float2(1.0, 0.0)), u.x),
		lerp(hash(i + float2(0.0, 1.0)),
			hash(i + float2(1.0, 1.0)), u.x), u.y);
}


//#define Unit  FlockData[id.x]
#define Neighbour  FlockData[i]
#define S(a) sin(a)
#define C(a) cos(a)
#define Rot2D(a) float2x2(C(a),-S(a),S(a),C(a))
#define N(a) noise(a)
#define pi acos(-1.)


float3 computeWander(in float3 pos ,float r) {


	float theta = (noise(pos.xz + _Time[1]) + 1.)*2. * 2 * pi;
	float phi = (noise(pos.yz) + _Time[1])*2. * pi;


	float3 wander = r*float3(cos(theta)*sin(phi), sin(theta)*cos(phi), cos(phi));
	return((wander));
}

[numthreads(GroupSize, 1, 1)]
void BoidMovementKernel(uint3 id : SV_DispatchThreadID)
{
	uint count = 1;
	FlockUnit Unit = FlockData[id.x];


	float3 uPos = Unit.position;
	float3 wander;
	float3 cohesion;
	float3 avoidance=float3(0.0001,0,0);
	float3 alignment = float3(0.000001,0,0);
	float cUnitSpeed = instanceLinearSpeed ;//Unit.speed;// / Unit.scale;//;+  (length(uPos)+ abs((noise(frac(_Time[3]) * uPos)))/10.)*unitSpeed;


	//FlockUnit Neighbour;
	for (uint i = 0; i <instanceCount ; i++)
	{
		if (i == id.x) continue;

		//Neighbour = FlockData[i];
		float d = distance(uPos, Neighbour.position);
		if (d <= (instancesSeparationRadius) && d!=0 )
		{
			
			float3 direction = uPos-Neighbour.position ;
			float directionLength = d;
			//float percent = 1.0 - directionLength / (instancesSeparationRadius + (Unit.scale+ Neighbour.scale)*0.5);
			float percent = 1.0 - directionLength / (instancesSeparationRadius );
			cohesion  += Neighbour.position;
			//avoidance += direction * (percent/directionLength);// *(1.0 - directionLength / (instancesSeparationRadius));
			avoidance += normalize(direction) * percent;
			alignment += Neighbour.direction;

			count+=1;
		}
		
	}

	

	float average = 1.0 / count;

	cohesion  *= average;
	cohesion = cohesion - Unit.position;
	cohesion = normalize(cohesion);
	alignment *= average;

	wander = computeWander(uPos,1+Unit.scale);
	wander.y = 0;
	
	float3 direction = 0.49*normalize(avoidance) ;// normalize(wander);// +normalize(cohesion + alignment + avoidance);
	direction += 0.3*normalize(wander);
	direction += 0.3*normalize(alignment);
	direction += 0.01*normalize(cohesion);

	//direction += normalize(wander);
	Unit.direction =lerp(direction,Unit.direction,exp(-instanceAngularSpeed/(Unit.scale*5) * deltaTime));
//	Unit.direction = direction;
	Unit.position +=  Unit.direction * deltaTime *cUnitSpeed;
	//Unit.speed = cUnitSpeed;
	Unit.speed =  float(count) / instanceCount;
	FlockData[id.x] = Unit;
	
}
